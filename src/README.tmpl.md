# {{theme}} Noble Theme

A theme for VS Code inspired by the noble gas {{theme}}.

![screenshot](https://gitlab.com/chaseadamsio/vscode-noble-themes/raw/master/screenshots/argon/argon-all.png)

## Getting Started

You can install this theme through the [Visual Studio Code Marketplace](https://marketplace.visualstudio.com/items?itemName=chaseadamsio.vscode-noble-theme-argon).

## Installation

Launch *Quick Open*

  - <img src="https://www.kernel.org/theme/images/logos/favicon.png" width=16 height=16/> <a href="https://code.visualstudio.com/shortcuts/keyboard-shortcuts-linux.pdf">Linux</a> `Ctrl+P`
  - <img src="https://developer.apple.com/favicon.ico" width=16 height=16/> <a href="https://code.visualstudio.com/shortcuts/keyboard-shortcuts-macos.pdf">macOS</a> `⌘P`
  - <img src="https://www.microsoft.com/favicon.ico" width=16 height=16/> <a href="https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf">Windows</a> `Ctrl+P`

Paste the following command and press `Enter`:

```shell
ext install vscode-noble-theme-argon
```

## Activate theme

Launch *Quick Open*

  - <img src="https://www.kernel.org/theme/images/logos/favicon.png" width=16 height=16/> <a href="https://code.visualstudio.com/shortcuts/keyboard-shortcuts-linux.pdf">Linux</a> `Ctrl+P`
  - <img src="https://developer.apple.com/favicon.ico" width=16 height=16/> <a href="https://code.visualstudio.com/shortcuts/keyboard-shortcuts-macos.pdf">macOS</a> `Shift+⌘+P`
  - <img src="https://www.microsoft.com/favicon.ico" width=16 height=16/> <a href="https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf">Windows</a> `Ctrl+P`

Type `theme` and choose `Preferences: Color Theme`, then select `Argon - Noble Theme` from the list.

## Screenshots

### Argon (Workbench + Terminal)

![Argon Full - Javascript](https://gitlab.com/chaseadamsio/vscode-noble-themes/raw/master/screenshots/argon/argon-all.png)

### Argon with Peek Definition

![Argon Full with Peek Definition - Javascript](https://gitlab.com/chaseadamsio/vscode-noble-themes/raw/master/screenshots/argon/argon-all-peek.png)

### Argon Flat (Workbench + Terminal)

![Argon Flat - Javascript](https://gitlab.com/chaseadamsio/vscode-noble-themes/raw/master/screenshots/argon/argon-flat.png)

### Argon Flat with Peek Definition

![Argon Flat with Peek Definition - Javascript](https://gitlab.com/chaseadamsio/vscode-noble-themes/raw/master/screenshots/argon/argon-flat-peek.png)

### Argon Editor + Workbench

![Argon Editor + Workbench - Javascript](https://gitlab.com/chaseadamsio/vscode-noble-themes/raw/master/screenshots/argon/argon-editor-workbench.png)

### Argon Editor Only

![Argon Editor Only - Javascript](https://gitlab.com/chaseadamsio/vscode-noble-themes/raw/master/screenshots/argon/argon-editor.png)
